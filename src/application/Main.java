package application;
	
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/*ChoiceBox - Ou DropDownMenu
 * Serve para escolher uma de v�rias op��es de uma lista
 * O obj � uma collection de JavaFX, que pode receber v�rios objetos de qualquer tipo
 * Passo1 - Criar a choise box collection de Strings
 * Passo2 - adicionar-lhe v�rias strings com as op��es pretendidas
 * Passo3 - Criar um bot�o para tratar da escolha
 * 
 * Nota: vamos precisar da alertBox de Utils*/

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
		
			
			ChoiceBox<String> choiceBox = new ChoiceBox<>();
			
			choiceBox.getItems().add("Op��o 1");
			choiceBox.getItems().add("Op��o 2");
			choiceBox.getItems().add("Op��o 3");
			choiceBox.getItems().addAll("Op��o 4", "Op��o 5");
			
			choiceBox.setValue("Op��o 3");
			
			Button btnShowoption = new Button("Mostra Op��o");
			btnShowoption.setOnAction(e -> application.Utils.alertBox("Sele��o", "Op��o Selecionada:\n"+choiceBox.getValue()));
			
			VBox layoutChoiceBox = new VBox(10);
			layoutChoiceBox.setPadding(new Insets(20,20,20,20));
			layoutChoiceBox.getChildren().addAll(choiceBox,btnShowoption);
			
			
			
			
			BorderPane root = new BorderPane();
			Scene scene = new Scene(layoutChoiceBox,300,200);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}